#ifndef VertexAttrib_h__
#define VertexAttrib_h__

#include <GL/glew.h>

namespace DLib
{
	struct VertexAttrib
	{
		VertexAttrib() {};
		~VertexAttrib() {};

		GLuint index;
		GLint size;
		GLenum type;
		GLboolean normalized;
		GLsizei stride;
		const GLvoid* offset;
	};
}

#endif // VertexAttrib_h__
