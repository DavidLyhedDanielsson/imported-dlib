#ifndef Obj_h__
#define Obj_h__

#include "content.h"

#include <vector>
#include "vertex3D.h"

namespace DLib
{
	class Obj : public Content
	{
	public:
		Obj();
		~Obj();

		std::vector<DLib::Vertex3D> vertices;
		std::vector<unsigned int> indices;

		bool operator==(Obj rhs);
		bool operator!=(Obj rhs);

        bool Load(const std::string& path, DLib::ContentManager* contentManager = nullptr, ContentParameters* contentParameters = nullptr);
        void Unload(DLib::ContentManager* contentManager = nullptr);

	private:
		void GetArgs(std::string args[4], const std::string& from);
		void SeparateArgs(std::string args[3], const std::string& from);
		std::string GetFirstArg(std::string from);
	};
}

#endif // Obj_h__
