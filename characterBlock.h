#ifndef CharacterBlock_h__
#define CharacterBlock_h__

#include "character.h"

#include <vector>

namespace DLib
{
	struct CharacterBlock
	{
		CharacterBlock() 
			: width(0)
			, utf8Length(0) {};
		CharacterBlock(const std::vector<const Character*>& characters, unsigned int width, unsigned int utf8Length)
			: characters(characters)
			, width(width)
			, utf8Length(utf8Length) {};
		~CharacterBlock() {};

		std::vector<const Character*> characters;
		unsigned int width;
		unsigned int utf8Length;
	};

	/*inline bool operator==(const CharacterBlock& lhs, const CharacterBlock& rhs)
	{
	if(lhs.width != rhs.width)
	return false;

	return lhs.text == rhs.text;
	}

	inline bool operator==(const CharacterBlock& lhs, const std::string& rhs)
	{
	return lhs.text == rhs;
	}

	inline bool operator !=(const CharacterBlock& lhs, const CharacterBlock& rhs)
	{
	return !(lhs == rhs);
	}

	inline bool operator!=(const CharacterBlock& lhs, const std::string& rhs)
	{
	return !(lhs == rhs);
	}*/
}

#endif // CharacterBlock_h__
