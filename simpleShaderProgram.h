#ifndef ShaderProgram_h__
#define ShaderProgram_h__

#include <GL/glew.h>

#include <string>
#include <unordered_map>
#include <vector>
#include <GL/gl.h>

#include "contentManager.h"
#include "fragmentShader.h"
#include "vertexShader.h"
#include "vertexAttrib.h"
#include "batchData.h"

namespace DLib
{
	class SimpleShaderProgram
	{
	public:
		SimpleShaderProgram();
		~SimpleShaderProgram();

		bool LoadProgram(ContentManager* contentManager, const std::string& vertexShaderPath, const std::string& fragmentShaderPath);
		void UnloadProgram(ContentManager* contentManager);

		void AddVertexAttrib(GLenum type, GLint size, GLint stride, GLboolean normalized = GL_FALSE);
		void SetProjectionMatrix(const glm::mat4x4& projectionMatrix);

		bool Bind() const;
		void Unbind() const;

		GLuint GetProgram() const;
		GLuint GetVertexArray() const;
	private:
		GLuint program;
		GLuint vertexArrayObject;

		VertexShader vertexShader;
		FragmentShader fragmentShader;

		std::vector<VertexAttrib> vertexAttribIndices;
		glm::mat4x4 projectionMatrix;

		size_t sizeofGLenum(GLenum glenum);
	};
}

#endif // ShaderProgram_h__
