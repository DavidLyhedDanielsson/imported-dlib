#include "characterSet.h"

#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>

#include "constructedString.h"
#include "spriteRenderer.h"

#include "texture2D.h"
#include "characterBlock.h"
#include <limits>

#include <utf8-cpp/utf8.h>

DLib::CharacterSet::CharacterSet()
{
	this->name = "";
	fontSize = 0;
}
/*
DLib::CharacterSet::CharacterSet(const DLib::CharacterSet& rhs)
	: name(rhs.name)
	, characters(rhs.characters)
	, fontSize(rhs.fontSize)
	, lineHeight(rhs.lineHeight)
	, spaceXAdvance(rhs.spaceXAdvance)
	, texture(rhs.texture)
{
}

DLib::CharacterSet::CharacterSet(const CharacterSet&& rhs)
	: name(std::move(rhs.name))
	, characters(std::move(rhs.characters))
	, fontSize(rhs.fontSize)
	, lineHeight(rhs.lineHeight)
	, spaceXAdvance(rhs.spaceXAdvance)
	, texture(rhs.texture)
{
	
}*/

DLib::CharacterSet::~CharacterSet()
{
}

//DLib::CharacterSet& DLib::CharacterSet::operator=(CharacterSet&& rhs)
//{
//	if(this != &rhs)
//	{
//		name = std::move(rhs.name);
//		characters = std::move(rhs.characters);
//
//		fontSize = rhs.fontSize;
//		lineHeight = rhs.lineHeight;
//		spaceXAdvance = rhs.spaceXAdvance;
//
//		texture = std::move(rhs.texture);
//	}
//
//	return *this;
//}

const DLib::Character* DLib::CharacterSet::GetCharacter(unsigned int id) const
{
	auto iter = characters.find(id);

	if(iter != characters.end())
		return &iter->second;
	else
	{
		iter = characters.find(errorCharacterID);

		if(iter != characters.end())
			return &iter->second;
		else
		{
			DLib::Logger::LogLine(DLib::LOG_TYPE_WARNING, "CharacterSet::errorCharacterID set to a non-existing character (make sure CharacterSet is loaded)");
			return &characters.begin()->second;
		}
	}
}

void DLib::CharacterSet::XMLSubscriber(const XMLElement& element)
{
	std::string elementName(element.GetName());

	if(elementName == "info")
	{
		fontSize = element.GetAttribute("size").GetValueAsUnsignedInt();
	}
	else if(elementName == "common")
	{
		lineHeight = element.GetAttribute("lineHeight").GetValueAsUnsignedInt();
	}
	else if(elementName == "char")
	{
		Character newChar(element.GetAttribute("id").GetValueAsUnsignedChar()
			, element.GetAttribute("x").GetValueAsShort()
			, element.GetAttribute("y").GetValueAsShort()
			, element.GetAttribute("width").GetValueAsChar()
			, element.GetAttribute("height").GetValueAsChar()
			, element.GetAttribute("xoffset").GetValueAsChar()
			, element.GetAttribute("yoffset").GetValueAsChar()
			, element.GetAttribute("xadvance").GetValueAsShort());

		characters.insert(std::pair<unsigned short, Character>(newChar.id, newChar));
	}
	else if(elementName == "kerning")
	{
        //unsigned char first = element.GetAttribute("first").GetValueAsUnsignedChar();
        //unsigned char second = element.GetAttribute("second").GetValueAsUnsignedChar();
        //char amount = element.GetAttribute("amount").GetValueAsChar();

		//characters[first].kerningPairs.insert(std::pair<unsigned char, char>(second, amount));
	}
}

std::string DLib::CharacterSet::GetName() const
{
	return name;
}

unsigned int DLib::CharacterSet::GetFontSize() const
{
	return fontSize;
}

DLib::ConstructedString DLib::CharacterSet::ConstructString(const std::string& text) const
{
	ConstructedString returnString;

	if(text == "")
	{
		//CharacterBlock newCharBlock;
		//charactersnewCharBlock.text = "";
		//newCharBlock.width = 0;

		std::vector<const Character*> newVector;
		returnString.characterBlocks.emplace_back(newVector, 0, 0);
		returnString.text = "";
		returnString.width = 0;
		returnString.utf8Length = 0;

		return returnString;
	}

	//Split each word (and the trailing blankspace) into a character block
	//If the string is something like "abc            def" split it into "abc" and " ... def" (ignoring a single space after abc)
	//A space is always presumed to be after a CharacterBlock when drawing
	bool splitAtSpace = false;

	unsigned int totalWidth = 0;

	CharacterBlock characterBlock;
	const Character* character;

	int utf8Length = 0;

	auto iter = text.begin(); //TODO: Turn into for?
	while(iter != text.end())
	{
		character = GetCharacter(utf8::unchecked::next(iter));

		if(character->id == SPACE_CHARACTER)
		{
			if(splitAtSpace)
			{
				splitAtSpace = false;

				returnString.characterBlocks.emplace_back(characterBlock);

				characterBlock.characters.clear();
				characterBlock.width = 0;
				characterBlock.utf8Length = 0;
				totalWidth += spaceXAdvance; //A space is always presumed to be after a character block so include it in the total width
				utf8Length++;
			}
			else
			{
				characterBlock.width += character->xAdvance;
				totalWidth += character->xAdvance;
				characterBlock.characters.emplace_back(character);
				characterBlock.utf8Length++;
				utf8Length++;
			}
		}
		else
		{
			splitAtSpace = true;

			characterBlock.width += character->xAdvance;
			totalWidth += character->xAdvance;
			characterBlock.characters.emplace_back(character);
			characterBlock.utf8Length++;
			utf8Length++;
		}
	}

	returnString.characterBlocks.emplace_back(characterBlock);

	returnString.width = totalWidth;
	returnString.text = text;
	returnString.utf8Length = utf8Length;

	return returnString;
}

DLib::ConstructedString DLib::CharacterSet::ConstructString(const char* text) const
{
	std::string textString(text);
	return ConstructString(textString);
}

void DLib::CharacterSet::Insert(DLib::ConstructedString& constructedString, unsigned int index, const std::string& string) const
{
    if(index > constructedString.text.size())
        index = constructedString.text.size();

	if(string.size() == 0)
		return;

	std::string stringToConstruct = "";

	int removeStart = 0;
	int removeDistance = 0;

    for(unsigned int currentIndex = 0, i = 0; i < constructedString.characterBlocks.size(); i++)
	{
        unsigned int blockLength = static_cast<unsigned int>(constructedString.characterBlocks[i].characters.size());

		if(currentIndex + blockLength <= index)
		{
			currentIndex += blockLength;

			if(currentIndex + 1 == index) //index is after the space after this character block.
			{
				//The current block will never be modified
				removeStart = i + 1; //Exclude current block from removal
				stringToConstruct += string;

				if(constructedString.characterBlocks.size() > i + 1) //It's probably more efficient to just add the next one instead of checking whether or not it's needed TODO: Maybe not?
				{
					stringToConstruct.reserve(constructedString.characterBlocks[i + 1].characters.size());
					for(const Character* character : constructedString.characterBlocks[i + 1].characters)
						utf8::unchecked::append(character->id, std::back_inserter(stringToConstruct));

					//stringToConstruct += constructedString.characterBlocks[i + 1].text;
					removeDistance++;
				}

				break;
			}
			else if(currentIndex == index) //index is before the space after this character block
			{
				removeStart = i;

				//The current character block will not be modified if "string" begins with a space
                auto iter = string.begin();
                if(utf8::unchecked::next(iter) != SPACE_CHARACTER)
				{
					stringToConstruct.reserve(constructedString.characterBlocks[i].characters.size());
					for(const Character* character : constructedString.characterBlocks[i].characters)
						utf8::unchecked::append(character->id, std::back_inserter(stringToConstruct));

					stringToConstruct += string;
					removeDistance++;
				}
				else
				{
					//Skip space
					auto iter = string.begin();
					utf8::unchecked::next(iter);

					stringToConstruct += string.substr(std::distance(string.begin(), iter) , string.npos); //Remove the space since it is between the current character block and the one that will be created
					removeStart++; //Exclude current block from removal
				}

				//The next block will be modified if "string" ends with a space
				if(string.back() == SPACE_CHARACTER
					&& constructedString.characterBlocks.size() > i + 1)
				{
					stringToConstruct += SPACE_CHARACTER;

					stringToConstruct.reserve(constructedString.characterBlocks[i + 1].characters.size());
					for(const Character* character : constructedString.characterBlocks[i + 1].characters)
						utf8::unchecked::append(character->id, std::back_inserter(stringToConstruct));
					//stringToConstruct += constructedString.characterBlocks[i + 1].text;
					removeDistance++;
				}

				break;
			}
			else
			{
				//constructedString.characterBlocks.emplace_back(std::move(characterBlocks[i]));
				currentIndex++; //Space
			}
		}
		else //index is in the middle of block (or first block).
		{
			//Only the current block will be modified no matter what
			removeStart = i;

			//auto iter = constructedString.characterBlocks[i].text.begin();
			//for(int j = 0; j < index - currentIndex; j++)
			//	utf8::unchecked::next(iter);

			stringToConstruct.reserve(constructedString.characterBlocks[i].characters.size() + string.size());
			auto iter = constructedString.characterBlocks[i].characters.begin();
            for(unsigned int j = 0; j < index - currentIndex; j++)
			{
				utf8::unchecked::append((*iter)->id, std::back_inserter(stringToConstruct));
				++iter;
			}
			//stringToConstruct += constructedString.characterBlocks[i].text.substr(0, std::distance(constructedString.characterBlocks[i].text.begin(), iter));
			stringToConstruct += string;
			while(iter != constructedString.characterBlocks[i].characters.end())
			{
				utf8::unchecked::append((*iter)->id, std::back_inserter(stringToConstruct));
				++iter;
			}
			//stringToConstruct += constructedString.characterBlocks[i].text.substr(std::distance(constructedString.characterBlocks[i].text.begin(), iter));
			removeDistance++;

			break;
		}
	}

	ConstructedString newString = ConstructString(stringToConstruct);

	constructedString.characterBlocks.reserve(constructedString.characterBlocks.size() + newString.characterBlocks.size());

	//Horray for move semantics!
	constructedString.characterBlocks.erase(constructedString.characterBlocks.begin() + removeStart, constructedString.characterBlocks.begin() + removeStart + removeDistance);
	constructedString.characterBlocks.insert(constructedString.characterBlocks.begin() + removeStart, newString.characterBlocks.begin(), newString.characterBlocks.end());

	auto iter = constructedString.text.begin();
    for(unsigned int i = 0; i < index; i++)
		utf8::unchecked::next(iter);

	constructedString.text.insert(iter, string.begin(), string.end());
	constructedString.width = 0;
	constructedString.utf8Length = 0;
	for(CharacterBlock block : constructedString.characterBlocks)
	{
		constructedString.width += block.width;
		constructedString.width += spaceXAdvance;
		constructedString.utf8Length += block.utf8Length;
		constructedString.utf8Length++;
	}

	if(constructedString.width > 0)
	{
		constructedString.width -= spaceXAdvance;
		constructedString.utf8Length--;
	}
}

void DLib::CharacterSet::Insert(DLib::ConstructedString& constructedString, int index, unsigned int character) const
{
	std::string string;
	utf8::unchecked::append(character, std::back_inserter(string));

	Insert(constructedString, index, string);
}

void DLib::CharacterSet::Erase(DLib::ConstructedString& constructedString, unsigned int startIndex, unsigned int count) const
{
	if(startIndex >= constructedString.utf8Length)
		return;

	if(startIndex + count > constructedString.utf8Length)
		count = constructedString.utf8Length - startIndex;

	//These steps are outdated since I remembered move semantics. TODO: Update them. Maybe.
	//1. Move character blocks from constructedString to temporary
	//2. Move character blocks that don't need to be modified from temporary back to constructedString until character block containing startIndex is found
	//3. Construct a string from first character block in range [startIndex, startIndex + count] as well as other blocks that may have to be modified
	//4. Construct a string from last character block in range [startIndex, startIndex + count] as well as other blocks that may have to be modified
	//5. Create a ConstructedString (not to be confused with a constructed string)
	//6. Move constructed string data into constructedString
	//7. Add rest of character block from temporary

	//Step 2 + 3
	std::string stringToConstruct = "";

	int removeStart = 0;
	int removeDistance = 0;

	unsigned int currentIndex = 0;
    unsigned int i = 0;
	for(; i < constructedString.characterBlocks.size(); i++)
	{
		unsigned int blockLength = static_cast<unsigned int>(constructedString.characterBlocks[i].characters.size());

		if(currentIndex + blockLength <= startIndex)
		{
			currentIndex += blockLength;

			if(currentIndex + 1 == startIndex) //startIndex is after the space after this character block
			{
				//The current block will never be modified
				//The next block will always be modified
				removeStart = i + 1;

				i++; //Start at next block
				currentIndex++;
				break;
			}
			else if(currentIndex == startIndex) //startIndex is before the space after this character block
			{
				//The current block may be modified
				//The next block will always be modified (or removed)
				removeStart = i;

				stringToConstruct.reserve(constructedString.characterBlocks[i].characters.size());
				for(const Character* character : constructedString.characterBlocks[i].characters)
					utf8::unchecked::append(character->id, std::back_inserter(stringToConstruct));
				//stringToConstruct += constructedString.characterBlocks[i].text; //Add this block since it may be modified since the trailing space is removed
				removeDistance++; //Since the current block may be modified

				currentIndex++;

				i++;
				break;
			}
			else
				currentIndex++; //Space
		}
		else //startIndex is in the middle of block (or first block)
		{
			//The current block will be modified
			//The next block may be modified
			removeStart = i;

			//auto iter = constructedString.characterBlocks[i].characters.begin();
			for(unsigned int j = 0; j < startIndex - currentIndex; j++)
				utf8::unchecked::append(constructedString.characterBlocks[i].characters[j]->id, std::back_inserter(stringToConstruct));

			break;
		}
	}/**/

	//Step 4
	for(; i < constructedString.characterBlocks.size(); i++)
	{
		unsigned int blockLength = static_cast<unsigned int>(constructedString.characterBlocks[i].characters.size());

		//Currently at character block
		if(currentIndex + blockLength > startIndex + count)
		{
			stringToConstruct.reserve(constructedString.characterBlocks[i].characters.size());
			for(auto iter = constructedString.characterBlocks[i].characters.begin() + (startIndex + count - currentIndex); iter != constructedString.characterBlocks[i].characters.end(); ++iter)
				utf8::unchecked::append((*iter)->id, std::back_inserter(stringToConstruct));

			removeDistance++;
			break;
		}
		else if(currentIndex + blockLength == startIndex + count) //Index is after character block, before space.
		{
			//If there is a block after this one it might be modified
			if(i + 1 < constructedString.characterBlocks.size())
			{
				stringToConstruct.reserve(constructedString.characterBlocks[i + 1].characters.size() + 1);
				stringToConstruct += SPACE_CHARACTER;
				for(auto iter = constructedString.characterBlocks[i + 1].characters.begin(); iter != constructedString.characterBlocks[i + 1].characters.end(); ++iter)
					utf8::unchecked::append((*iter)->id, std::back_inserter(stringToConstruct));
				removeDistance += 2;
			}
			else
				removeDistance++;

			break;
		}

		currentIndex += blockLength + 1; //+ 1 for space
		removeDistance++;
	}

	//Step 5
	ConstructedString newString = ConstructString(stringToConstruct);

	//Horray for move semantics (again)!
	constructedString.characterBlocks.erase(constructedString.characterBlocks.begin() + removeStart, constructedString.characterBlocks.begin() + removeStart + removeDistance);
	constructedString.characterBlocks.insert(constructedString.characterBlocks.begin() + removeStart, newString.characterBlocks.begin(), newString.characterBlocks.end());

	auto beginIter = constructedString.text.begin();
	utf8::unchecked::advance(beginIter, startIndex);

	auto endIter = beginIter;
	utf8::unchecked::advance(endIter, count);

	constructedString.text.erase(beginIter, endIter);
	constructedString.width = 0;
	constructedString.utf8Length = 0;
	for(CharacterBlock block : constructedString.characterBlocks)
	{
		constructedString.width += block.width;
		constructedString.width += spaceXAdvance;
		constructedString.utf8Length += block.utf8Length;
		constructedString.utf8Length++;
	}

	if(constructedString.width > 0)
	{
		constructedString.width -= spaceXAdvance;
		constructedString.utf8Length--;
	}
}

DLib::Texture2D* DLib::CharacterSet::GetTexture() const
{
	return texture;
}

bool DLib::CharacterSet::Load(const std::string& path, DLib::ContentManager* contentManager, ContentParameters* contentParameters)
{
	characters.clear();

	this->name = path;

	XMLFile xmlFile;
	xmlFile.Open(path + ".fnt");
	xmlFile.Parse(std::bind(&CharacterSet::XMLSubscriber, this, std::placeholders::_1));

	texture = contentManager->Load<DLib::Texture2D>(path + ".png");

	if(characters.size() > 0)
	{
		characters.insert(std::pair<unsigned short, Character>('\n', Character('\n', 0, 0, 0, fontSize + lineHeight, 0, 0, 0)));

		//If the user expects to use spaces it is expected to be in the .xml-file
		spaceXAdvance = static_cast<unsigned int>(GetCharacter(SPACE_CHARACTER)->xAdvance);
	}

	return true;
}

void DLib::CharacterSet::Unload(DLib::ContentManager* contentManager)
{
	if(texture != nullptr)
		contentManager->Unload(texture);
}

unsigned int DLib::CharacterSet::GetLineHeight() const
{
	return lineHeight;
}

unsigned int DLib::CharacterSet::GetWidthAtMaxWidth(const DLib::ConstructedString& string, unsigned int maxWidth) const
{
	unsigned int currentWidth = 0;

	for(const CharacterBlock& block : string.characterBlocks)
	{
		if(currentWidth + block.width > maxWidth)
		{
			short characterXAdvance;

			for(const Character* character : block.characters)
			{
				characterXAdvance = character->xAdvance;

				currentWidth += characterXAdvance;

				if(currentWidth >= maxWidth)
					return currentWidth - characterXAdvance;
			}
		}
		else
			currentWidth += block.width;
	}

	return currentWidth;
}

unsigned int DLib::CharacterSet::GetWidthAtIndex(const DLib::ConstructedString& constructedString, int index) const
{
	if(index <= 0)
		return 0;
	else if(index >= utf8::unchecked::distance(constructedString.text.begin(), constructedString.text.end()))
		return constructedString.width;

	int currentWidth = 0;
	int currentIndex = 0;

	for(const CharacterBlock& block : constructedString.characterBlocks)
	{
		int blockSize = static_cast<int>(block.characters.size());

		//Is the index inside the current block?
		if(currentIndex + blockSize > index)
		{
			for(int i = 0; i < index - currentIndex; i++)
				currentWidth += block.characters[i]->xAdvance;

			return currentWidth;
		}
		else if(currentIndex + blockSize == index) //TODO: Remove this if?
		{
			return currentWidth + block.width;
		}
		else
		{
			currentWidth += block.width;
			currentIndex += blockSize;
		}

		currentWidth += spaceXAdvance;
		currentIndex++;

		if(currentIndex == index)
			return currentWidth;
	}

	return currentWidth;
}

unsigned int DLib::CharacterSet::GetIndexAtWidth(const DLib::ConstructedString& constructedString, int width, float roundingValue /*= 0.6f*/) const
{
	if(width <= 0)
		return 0;
	else if(width >= static_cast<int>(constructedString.width))
		return constructedString.utf8Length;

	int currentWidth = 0;
	int currentIndex = 0;

	//Used for index rounding
	int lastCharXAdvance = 0;

	for(const DLib::CharacterBlock& characterBlock : constructedString.characterBlocks)
	{
		if(currentWidth + static_cast<int>(characterBlock.width) < width)
		{
			currentWidth += characterBlock.width;
			currentIndex += static_cast<unsigned int>(characterBlock.characters.size());
		}
		else
		{
			for(const DLib::Character* character : characterBlock.characters)
			{
				if(currentWidth + character->xAdvance <= width)
				{
					currentWidth += character->xAdvance;
					currentIndex++;
				}
				else
				{
					lastCharXAdvance = character->xAdvance;
					break;
				}
			}

			break;
		}



		//Space
		if(currentWidth + static_cast<int>(spaceXAdvance) <= width)
		{
			currentWidth += spaceXAdvance;
			currentIndex++;
		}
		else
		{
			lastCharXAdvance = spaceXAdvance;
			break;
		}
	}

	currentIndex += (currentWidth + lastCharXAdvance - width) < (lastCharXAdvance * roundingValue);

	return currentIndex;
}

unsigned int DLib::CharacterSet::GetRowsAtWidth(const DLib::ConstructedString& constructedString, unsigned int width) const
{
	unsigned int rows = 1;
	unsigned int currentWidth = 0;

	for(const DLib::CharacterBlock& block : constructedString.characterBlocks)
	{
		if(currentWidth + block.width <= width)
		{
			currentWidth += block.width + GetSpaceXAdvance();
		}
		else
		{
			if(block.width > width)
			{
				//Block needs to be split into several lines

				for(const DLib::Character* character : block.characters)
				{
					if(currentWidth + character->xAdvance < width)
						currentWidth += character->xAdvance;
					else
					{
						currentWidth = static_cast<unsigned int>(character->xAdvance);
						rows++;
					}
				}

				currentWidth += GetSpaceXAdvance();
			}
			else
			{
				currentWidth = block.width + GetSpaceXAdvance();
				rows++;
			}
		}
	}

	return rows;
}

int DLib::CharacterSet::GetSpaceXAdvance() const
{
	return spaceXAdvance;
}