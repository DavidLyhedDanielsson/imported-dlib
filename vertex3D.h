#ifndef Vertex3D_h__
#define Vertex3D_h__

#include "glm/glm.hpp"

#include <algorithm>

namespace DLib
{
	struct Vertex3D
	{
	public:
		Vertex3D();
		~Vertex3D();

		glm::vec3 position;
		glm::vec3 normal;
		glm::vec2 texCoords;

		bool operator==(Vertex3D rhs) const;
		bool operator!=(Vertex3D rhs) const;
	};
}
#endif // Vertex3D_h__
