#ifndef SpriteRenderer_h__
#define SpriteRenderer_h__

#include <string>
#include <vector>

#include <glm/glm.hpp>

#include "rect.h"
#include "texture2D.h"
#include "contentManager.h"
#include "vertex2D.h"
#include "spriteInfo.h"
#include "simpleShaderProgram.h"

#include "characterSet.h"
#include "spriteBatch.h"

using std::string;

namespace DLib
{
	class SpriteRenderer
	{
	public:
		SpriteRenderer();
		~SpriteRenderer();

		void Init(DLib::ContentManager* contentManager, int xRes, int yRes);

		void Begin();
		void End();

		//void DrawString(const Texture2D& texture2D, const glm::vec2& position, const ConstructedString& characters, const glm::vec4& color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f), unsigned int maxRowLength = 0xFFFFFFFF);

		void DrawString(const DLib::CharacterSet* characterSet, const DLib::ConstructedString& text, glm::vec2 position, const glm::vec4 color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
		void DrawString(const DLib::CharacterSet* characterSet, const DLib::ConstructedString& text, glm::vec2 position, int maxWidth, const glm::vec4 color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
		void DrawString(const DLib::CharacterSet* characterSet, const DLib::ConstructedString& text, glm::vec2 position, unsigned int startIndex, unsigned int count, const glm::vec4& color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));

		void Draw(const DLib::Texture2D& texture2D, const DLib::Rect& drawRect, const glm::vec4& color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
		void Draw(const DLib::Texture2D& texture2D, const glm::vec2& position, const DLib::Rect& clipRect, const glm::vec4& color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
		void Draw(const DLib::Texture2D& texture2D, const DLib::Rect& position, const DLib::Rect& clipRect, const glm::vec4& color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
		void Draw(const DLib::Rect& drawRect, const glm::vec4& color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
		void Draw();

		void Reload();

		void EnableScissorTest(DLib::Rect region);
		void DisableScissorTest();

		void SetResolution(int xRes, int yRes);

		static Texture2D* const GetWhiteTexture();
		static DLib::Rect GetWhiteTextureClipRect();
	private:
		DLib::ContentManager* contentManager;

		bool hasBegun;
		//////////////////////////////////////////////////////////////////////////
		//BUFFER VARIABLES
		//////////////////////////////////////////////////////////////////////////
		GLuint vertexBuffer;
		GLuint elementBuffer;

		SimpleShaderProgram shaderProgram;

		const int MAX_BUFFER_INSERTS = 2048;

		//Vertex2D = 32 bytes (with padding)
		//65536 * 4 * 32 = 8388608 = 8 MiB
		const int MAX_GRAPHICS_BUFFER_INSERTS = 65536;
		const int MAX_VERTEX_BUFFER_INSERTS = MAX_GRAPHICS_BUFFER_INSERTS * 4;
		const int MAX_VERTEX_BUFFER_SIZE = MAX_VERTEX_BUFFER_INSERTS * sizeof(Vertex2D);
		const int MAX_ELEMENT_BUFFER_INSERTS = MAX_GRAPHICS_BUFFER_INSERTS * 6;
		const int MAX_ELEMENT_BUFFER_SIZE = MAX_ELEMENT_BUFFER_INSERTS * sizeof(GLuint);

		GLuint vertexBufferArrayInserts;
		GLuint bufferInserts;

		std::vector<SpriteBatch> spriteBatch;

		GLuint vertexDataStreamOffset;
		GLuint indexDataStreamOffset;
		GLuint drawOffset;

		//For easy drawing of rectangles via Draw()
		static Texture2D* whiteTexture;
		static DLib::Rect whiteTextureClipRect;

		DLib::Rect resolutionRect;

		GLuint currentTexture;
	};
}
#endif // SpriteRenderer_h__
