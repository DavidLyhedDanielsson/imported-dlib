#ifndef BatchData_h__
#define BatchData_h__

#include <glm/glm.hpp>

namespace DLib
{
	class BatchData
	{
	public:
		BatchData()
			:positionMin(0, 0)
			, positionMax(0, 0)
			, texCoordsMin(0, 0)
			, texCoordsMax(0, 0)
			, color(0, 0, 0, 0)
		{};
		BatchData(const glm::vec2& positionMin
			, const glm::vec2& positionMax
			, const glm::vec2& texCoordsMin
			, const glm::vec2& texCoordsMax
			, const glm::vec4& color)
			:positionMin(positionMin)
			, positionMax(positionMax)
			, texCoordsMin(texCoordsMin)
			, texCoordsMax(texCoordsMax)
			, color(color)
		{};
		~BatchData() {};

		glm::vec2 positionMin;
		glm::vec2 positionMax;

		glm::vec2 texCoordsMin;
		glm::vec2 texCoordsMax;

		glm::vec4 color;
	};
}

#endif // BatchData_h__
