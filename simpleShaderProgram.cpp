#include "simpleShaderProgram.h"

#include "logger.h"

#include <memory>
#include <glm/gtc/type_ptr.hpp>
#include <GL/gl.h>
//#include <glob.h>

DLib::SimpleShaderProgram::SimpleShaderProgram()
	:program(0)
	, vertexShader()
	, fragmentShader()
{
}

DLib::SimpleShaderProgram::~SimpleShaderProgram()
{
}

bool DLib::SimpleShaderProgram::LoadProgram(ContentManager* contentManager, const std::string& vertexShaderPath, const std::string& fragmentShaderPath)
{
	glGenVertexArrays(1, &vertexArrayObject);
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		Logger::LogLine(DLib::LOG_TYPE_ERROR, "Error while generating vertex array for simple shader program: \"" + std::string(reinterpret_cast<const char*>(gluGetString(error))) + "\"");
		return false;
	}

	glBindVertexArray(vertexArrayObject);

	vertexShader = *contentManager->Load<DLib::VertexShader>(vertexShaderPath);
	if(vertexShader.GetShader() == 0)
	{
		UnloadProgram(contentManager);
		return false;
	}

	fragmentShader = *contentManager->Load<DLib::FragmentShader>(fragmentShaderPath);
	if(fragmentShader.GetShader() == 0)
	{
		UnloadProgram(contentManager);
		return false;
	}

	program = glCreateProgram();
	if(program == 0)
	{
		Logger::LogLine(DLib::LOG_TYPE_ERROR, "Couldn't create shader program!");
		UnloadProgram(contentManager);
		return false;
	}

	glAttachShader(program, vertexShader.GetShader());
	glAttachShader(program, fragmentShader.GetShader());
	glLinkProgram(program);
	glUseProgram(program);

	GLint linkSuccess = GL_TRUE;
	glGetProgramiv(program, GL_LINK_STATUS, &linkSuccess);
	if(linkSuccess != GL_TRUE)
	{
		GLint maxLength = 0;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

		GLint logLength = 0;
		GLchar* log = new char[maxLength];
		glGetShaderInfoLog(program, maxLength, &logLength, log);
		if(logLength > 0)
			Logger::LogLine(DLib::LOG_TYPE_ERROR, log);
		else
			Logger::LogLine(DLib::LOG_TYPE_ERROR, "An error occured while linking shader program but no log was available!");

		delete[] log;
		UnloadProgram(contentManager);

		return false;
	}

	for(VertexAttrib attrib : vertexAttribIndices)
	{
		glEnableVertexAttribArray(attrib.index);

		switch(attrib.type)
		{
			case GL_BYTE:
			case GL_UNSIGNED_BYTE:
			case GL_SHORT:
			case GL_UNSIGNED_SHORT:
			case GL_INT:
			case GL_UNSIGNED_INT:
				glVertexAttribIPointer(attrib.index, attrib.size, attrib.type, attrib.stride, attrib.offset);
				break;
			case GL_DOUBLE:
				glVertexAttribLPointer(attrib.index, attrib.size, attrib.type, attrib.stride, attrib.offset);
				break;
			default:
				glVertexAttribPointer(attrib.index, attrib.size, attrib.type, attrib.normalized, attrib.stride, attrib.offset);
				break;
		}

		error = glGetError();
		if(error != GL_NO_ERROR)
		{
			const GLubyte* glErrorMessage = gluErrorString(error);
			Logger::LogLine(DLib::LOG_TYPE_ERROR, "VertexAttrib error! " + std::string(reinterpret_cast<const char*>(glErrorMessage)));

			UnloadProgram(contentManager);
			return false;
		}
	}

	GLint propjectionMatirxLocation = glGetUniformLocation(program, "projectionMatrix");
	glUniformMatrix4fv(propjectionMatirxLocation, 1, GL_FALSE, glm::value_ptr(projectionMatrix));

	glBindVertexArray(0);
	glUseProgram(0);

	return true;
}

void DLib::SimpleShaderProgram::UnloadProgram(DLib::ContentManager* contentManager)
{
	contentManager->Unload(&vertexShader);
	contentManager->Unload(&fragmentShader);

	if(program != 0)
		glDeleteProgram(program);
	if(vertexArrayObject != 0)
		glDeleteBuffers(1, &vertexArrayObject);
}

void DLib::SimpleShaderProgram::AddVertexAttrib(GLenum type, GLint size, GLint stride, GLboolean normalized/* = GL_FALSE*/)
{
	VertexAttrib newAttrib;

	if(sizeofGLenum(type) == 0)
	{
		DLib::Logger::LogLine(LOG_TYPE_WARNING, "Error when adding vertex attrib to simple shader program: unknown type");
		return;
	}

	newAttrib.type = type;
	newAttrib.normalized = normalized;
	newAttrib.size = size;
	newAttrib.stride = stride;
	newAttrib.index = static_cast<GLint>(vertexAttribIndices.size());

	if(vertexAttribIndices.size() > 0)
		newAttrib.offset = (void*)((uint64_t)(vertexAttribIndices.back().offset) + vertexAttribIndices.back().size * sizeofGLenum(vertexAttribIndices.back().type));
	else
		newAttrib.offset = 0;

	vertexAttribIndices.emplace_back(newAttrib);
}

void DLib::SimpleShaderProgram::SetProjectionMatrix(const glm::mat4x4& projectionMatrix)
{
	this->projectionMatrix = projectionMatrix;
}

bool DLib::SimpleShaderProgram::Bind() const
{
	glUseProgram(program);
	glBindVertexArray(vertexArrayObject);

	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
        LogLineWithFileData(DLib::LOG_TYPE_ERROR, "Error when binding simple shader program: " + std::string(reinterpret_cast<const char*>(gluErrorString(error))));
		return false;
	}

	return true;
}

void DLib::SimpleShaderProgram::Unbind() const
{
	glUseProgram(0);
	glBindVertexArray(0);
}

GLuint DLib::SimpleShaderProgram::GetProgram() const
{
	return program;
}

GLuint DLib::SimpleShaderProgram::GetVertexArray() const
{
	return vertexArrayObject;
}

size_t DLib::SimpleShaderProgram::sizeofGLenum(GLenum glenum)
{
	switch(glenum)
	{
		case GL_BYTE:
		case GL_UNSIGNED_BYTE:
			return sizeof(GLbyte);
		case GL_SHORT:
		case GL_UNSIGNED_SHORT:
		case GL_HALF_FLOAT:
			return sizeof(GLshort);
		case GL_INT:
		case GL_UNSIGNED_INT:
		case GL_FIXED:
			return sizeof(GLint);
		case GL_FLOAT:
			return sizeof(GLfloat);
		case GL_DOUBLE:
			return sizeof(GLdouble);
		default:
			return 0;
	}
}
