#include "xmlAttribute.h"

DLib::XMLAttribute::XMLAttribute()
	: name("")
	, value("")
{
}

DLib::XMLAttribute::XMLAttribute(const std::string& name, const std::string& value)
	:name(name)
	,value(value)
{

}

DLib::XMLAttribute::~XMLAttribute()
{
}

std::string DLib::XMLAttribute::GetName() const
{
	return name;
}

short DLib::XMLAttribute::GetValueAsShort() const
{
	return static_cast<short>(stoi(value));
}

int DLib::XMLAttribute::GetValueAsInt() const
{
	return stoi(value);
}

unsigned int DLib::XMLAttribute::GetValueAsUnsignedInt() const
{
	return static_cast<unsigned int>(stol(value));
}

long DLib::XMLAttribute::GetValueAsLong() const
{
	return stol(value);
}

unsigned long DLib::XMLAttribute::GetValueAsUnsignedLong() const
{
	return stoul(value);
}

long long DLib::XMLAttribute::GetValueAsLongLong() const
{
	return stoll(value);
}

unsigned long long DLib::XMLAttribute::GetValueAsUnsignedLongLong() const
{
	return stoull(value);
}

float DLib::XMLAttribute::GetValueAsFloat() const
{
	return stof(value);
}

double DLib::XMLAttribute::GetValueAsDouble() const
{
	return stod(value);
}

char DLib::XMLAttribute::GetValueAsChar() const
{
	return static_cast<char>(stoi(value));
}

unsigned char DLib::XMLAttribute::GetValueAsUnsignedChar() const
{
	return static_cast<unsigned char>(stoi(value));
}

std::string DLib::XMLAttribute::GetValueAsString() const
{
	return value;
}
