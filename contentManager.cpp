#include "contentManager.h"

#include <fstream>
#include <string>
#include <algorithm>

#include "logger.h"

#include "xmlFile.h"

DLib::ContentManager::ContentManager()
{

}

DLib::ContentManager::~ContentManager()
{
	Unload();
}

void DLib::ContentManager::Unload()
{
	for(auto iter : contentMap)
	{
		iter.second->Unload(this);
	}

	for(auto iter : contentMap)
	{
		delete iter.second;
		iter.second = nullptr;
	}

	contentMap.clear();
}

void DLib::ContentManager::Unload(const std::string& path)
{
	auto iter = contentMap.find(path);
	if(iter->second == nullptr)
		return; //Already unloaded

	if(iter == contentMap.end())
	{
		DLib::Logger::LogLine(DLib::LOG_TYPE_WARNING, "Trying to unload conten that has already been unloaded or hasn't been loaded at all");
		return;
	}

	iter->second->refCount--;
	if(iter->second->refCount > 0)
		return;

	iter->second->Unload(this);
	iter->second = nullptr;
	delete iter->second;

	contentMap.erase(iter->first);
}

void DLib::ContentManager::Unload(Content* content)
{
	std::string path = content->path;

	if(path == "") //"Local" content, it's not in contentMap
	{
		DLib::Logger::LogLine(DLib::LOG_TYPE_WARNING, "Trying to unload content where path = "". Make sure to only load content through ContentManager");
		return;
	}

	auto iter = contentMap.find(path);

	if(iter->second == nullptr)
		return; //Already unloaded

	iter->second->refCount--;
	if(iter->second->refCount > 0)
		return; //This content is used somewhere else

	iter->second->Unload(this);
	delete contentMap[iter->first];

	contentMap.erase(path);
}