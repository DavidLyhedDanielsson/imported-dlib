#ifndef Texture2DCreateParameters_h__
#define Texture2DCreateParameters_h__

#include "contentCreationParameters.h"
#include "texture2DParameters.h"

#include <GL/glew.h>
#include <array>

namespace DLib
{
	struct Texture2DCreateParameters :
		public ContentCreationParameters
	{
		Texture2DCreateParameters()
			: data(nullptr)
			, width(-1)
			, height(-1)
		{

		}

		~Texture2DCreateParameters() {};

		Texture2DParameters texParams;

		unsigned char* data;

		int width;
		int height;
		//Refer to SOIL_create_OGL_texture()
		int channels;
	};
}

#endif // Texture2DCreateParameters_h__
