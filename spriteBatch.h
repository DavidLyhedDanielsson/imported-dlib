#ifndef _OPENGLWINDOW_SPRITEBATCH_H_
#define _OPENGLWINDOW_SPRITEBATCH_H_

#include "batchData.h"

#include <GL/glew.h>
#include <vector>

namespace DLib
{
    struct SpriteBatch
    {
    public:
        SpriteBatch()
            : texture(0)
        {}
        SpriteBatch(GLuint texture)
            : texture(texture)
        {}
        SpriteBatch(GLuint texture, const DLib::BatchData& firstElement)
            : SpriteBatch(texture)
        {
            batchData.emplace_back(firstElement);
        }

        ~SpriteBatch() {}

        GLuint texture;
        std::vector<DLib::BatchData> batchData;
    };
}


#endif //_OPENGLWINDOW_SPRITEBATCH_H_
