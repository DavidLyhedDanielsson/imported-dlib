#ifndef Texture2DParameters_h__
#define Texture2DParameters_h__

#include "contentParameters.h"

#include <GL/glew.h>

#include <glm/vec4.hpp>
#include <array>

namespace DLib
{
	struct Texture2DParameters :
		public ContentParameters
	{
		Texture2DParameters()
			:depthStencilTextureMode(GL_DEPTH_COMPONENT)
			, baseLevel(0)
			, textureBorderColor(0.0f, 0.0f, 0.0f, 0.0f)
			, compareFunc(GL_NEVER)
			, compareMode(GL_NONE)
			, lodBias(0.0f)
			, minFiler(GL_LINEAR_MIPMAP_LINEAR)
			, magFiler(GL_LINEAR)
			, minLOD(-1000)
			, maxLOD(1000)
			, swizzleR(GL_RED)
			, swizzleG(GL_GREEN)
			, swizzleB(GL_BLUE)
			, swizzleA(GL_ALPHA)
			, wrapS(GL_REPEAT)
			, wrapT(GL_REPEAT)
			, wrapR(GL_REPEAT) 
		{
			//Can't put this in init. list
			swizzleRGBA = { { GL_RED, GL_GREEN, GL_BLUE, GL_ALPHA } };
		}
		~Texture2DParameters() {};

		/*GL_DEPTH_STENCIL_TEXTURE_MODE = GL_DEPTH_COMPONENT*/
		GLint depthStencilTextureMode;
		/*GL_TEXTURE_BASE_LEVEL = 0*/
		GLint baseLevel;
		/*GL_TEXTURE_BORDER_COLOR = (0.0f, 0.0f, 0.0f, 0.0f)*/
		glm::vec4 textureBorderColor;
		/*GL_TEXTURE_COMPARE_FUNC = GL_NEVER*/
		GLint compareFunc;
		/*GL_TEXTURE_COMPARE_MODE = GL_NONE*/
		GLint compareMode;
		/*GL_TEXTURE_LOD_BIAS = 0.0f*/
		GLfloat lodBias;
		/*GL_TEXTURE_MIN_FILTER = GL_LINEAR_MIPMAP_LINEAR*/
		GLint minFiler;
		/*GL_TEXTURE_MAG_FILTER = GL_LINEAR*/
		GLint magFiler;
		/*GL_TEXTURE_MIN_LOD = -1000*/
		GLint minLOD;
		/*GL_TEXTURE_MAX_LOD = 1000*/
		GLint maxLOD;
		/*GL_TEXTURE_MAX_LEVEL = 1000*/
		GLint maxLevel;
		/*GL_TEXTURE_SWIZZLE_R = GL_RED*/
		GLint swizzleR;
		/*GL_TEXTURE_SWIZZLE_G = GL_GREEN*/
		GLint swizzleG;
		/*GL_TEXTURE_SWIZZLE_B = GL_BLUE*/
		GLint swizzleB;
		/*GL_TEXTURE_SWIZZLE_A = GL_ALPHA*/
		GLint swizzleA;
		/*GL_TEXTURE_SWIZZLE_RGBA = { GL_RED, GL_GREEN, GL_BLUE, GL_ALPHA }*/
		std::array<int, 4> swizzleRGBA;
		/*GL_TEXTURE_WRAP_S = GL_REPEAT*/
		GLint wrapS;
		/*GL_TEXTURE_WRAP_T = GL_REPEAT*/
		GLint wrapT;
		/*GL_TEXTURE_WRAP_R = GL_REPEAT*/
		GLint wrapR;
	};
}

#endif // Texture2DParameters_h__
