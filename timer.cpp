#include "timer.h"

DLib::Timer::Timer()
	:isRunning(false)
{
	currentTime = std::chrono::high_resolution_clock::now();

	previousTime = currentTime;
	creationTime = currentTime;
	stopTime = currentTime;

	runTime = std::chrono::high_resolution_clock::duration::zero();
	pauseTime = std::chrono::high_resolution_clock::duration::zero();
	deltaTime = std::chrono::high_resolution_clock::duration::zero();
}

DLib::Timer::~Timer() {}

void DLib::Timer::UpdateDelta()
{
	currentTime = std::chrono::high_resolution_clock::now();

	deltaTime = currentTime - previousTime;
	previousTime = currentTime;
}

void DLib::Timer::ResetDelta()
{
	deltaTime = std::chrono::high_resolution_clock::duration::zero();
	previousTime = std::chrono::high_resolution_clock::now();
}

void DLib::Timer::Start()
{
	if(!isRunning)
	{
		pauseTime += std::chrono::high_resolution_clock::now() - stopTime;

		isRunning = true;
	}
}

void DLib::Timer::Stop()
{
	if(isRunning)
	{
		stopTime = std::chrono::high_resolution_clock::now();

		isRunning = false;
	}
}

void DLib::Timer::Reset()
{
	currentTime = std::chrono::high_resolution_clock::now();

	previousTime = currentTime;
	creationTime = currentTime;
	stopTime = currentTime;

	runTime = std::chrono::high_resolution_clock::duration::zero();
	pauseTime = std::chrono::high_resolution_clock::duration::zero();
	deltaTime = std::chrono::high_resolution_clock::duration::zero();
}

std::chrono::high_resolution_clock::duration DLib::Timer::GetTime() const
{
	if(isRunning)
		return std::chrono::high_resolution_clock::now() - creationTime - pauseTime;
	else
		return stopTime - creationTime - pauseTime;
}

std::chrono::high_resolution_clock::duration DLib::Timer::GetDelta() const
{
	return deltaTime;
}

long long DLib::Timer::GetTimeNanoseconds() const
{
	return std::chrono::duration_cast<std::chrono::nanoseconds>(GetTime()).count();
}

long long DLib::Timer::GetTimeMicroseconds() const
{
	return std::chrono::duration_cast<std::chrono::microseconds>(GetTime()).count();
}

long long DLib::Timer::GetTimeMilliseconds() const
{
	return std::chrono::duration_cast<std::chrono::milliseconds>(GetTime()).count();
}

float DLib::Timer::GetTimeMillisecondsFractal() const
{
	return std::chrono::duration_cast<std::chrono::nanoseconds>(GetTime()).count() * 0.000001f;
}

bool DLib::Timer::IsRunning() const
{
	return isRunning;
}
