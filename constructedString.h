#ifndef ConstructedString_h__
#define ConstructedString_h__

#include <vector>
#include <string>

#include "characterBlock.h"

namespace DLib
{
	struct ConstructedString
	{
		ConstructedString() 
			: width(0)
			, text("")
			, utf8Length(0)
		{};
		ConstructedString(std::vector<CharacterBlock> characterBlocks, unsigned int width, std::string text, unsigned int utf8Length)
			: characterBlocks(characterBlocks)
			, width(width)
			, text(text)
			, utf8Length(utf8Length) {};
		~ConstructedString() {};

		std::vector<CharacterBlock> characterBlocks;
		unsigned int width;
		std::string text;
		unsigned int utf8Length;
	};

	inline bool operator==(const ConstructedString& lhs, const std::string& rhs)
	{
		return lhs.text == rhs;
	}

	inline bool operator!=(const ConstructedString& lhs, const std::string& rhs)
	{
		return !(lhs == rhs);
	}

	inline bool operator==(const std::string& lhs, const ConstructedString& rhs)
	{
		return rhs == lhs;
	}

	inline bool operator!=(const std::string& lhs, const ConstructedString& rhs)
	{
		return !(rhs == lhs);
	}
}

#endif // ConstructedString_h__
