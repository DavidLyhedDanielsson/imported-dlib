#include "logger.h"

#include <iostream>
#include <fstream>
#include <chrono>
#include <iomanip>

#ifdef __linux__
#include <execinfo.h>
bool DLib::Logger::printStackTrace = true;
#endif

DLib::CONSOLE_LOG_LEVEL DLib::Logger::consoleLogLevel = DLib::LOG_LEVEL_PARTIAL;
std::string DLib::Logger::outPath = "";
std::string DLib::Logger::outName = "log.txt";
std::ios_base::openmode DLib::Logger::openMode = std::ios_base::app;
std::string DLib::Logger::separatorString = " ";

//DLib::Logger::Logger()
//{
//
//}
//
//DLib::Logger::~Logger()
//{
//}

void DLib::Logger::LogLineWithFileDataF(const char *file, int line, LOG_TYPE logType, const std::string& text)
{
    std::string message = "File \"";
    message += file;
    message += "\", line ";
    message += std::to_string(line);
    message += ": ";
    message += text;

    LogLine(logType, message);
}

void DLib::Logger::LogLineWithFileDataF(const char *file, int line, LOG_TYPE logType, const char* text)
{
    LogLineWithFileDataF(file, line, logType, std::string(text));
}

void DLib::Logger::LogWithFileDataF(const char* file, int line, LOG_TYPE logType, const std::string& text)
{
    std::string message = "File \"";
    message += file;
    message += "\", line ";
    message += std::to_string(line);
    message += ": ";
    message += text;

    Log(logType, message);
}

void DLib::Logger::LogWithFileDataF(const char* file, int line, LOG_TYPE logType, const char* text)
{
    LogWithFileDataF(file, line, logType, std::string(text));
}

void DLib::Logger::LogLine(LOG_TYPE logType, const std::string& text)
{
    Log(logType, text + "\n");
}

void DLib::Logger::LogLine(LOG_TYPE logType, const char* text)
{
    Log(logType, std::string(text) + "\n");
}

void DLib::Logger::Log(LOG_TYPE logType, const std::string& text)
{
	std::string message("");

	switch(logType)
	{
		case LOG_TYPE_NONE:
			break;
		case LOG_TYPE_INFO:
			message += "[INFO]";
			break;
		case LOG_TYPE_WARNING:
			message += "[WARNING]";
            break;
		case LOG_TYPE_ERROR:
			message += "[ERROR]";
			break;
		default:
			break;
	}

	//Only log error type to console
	//Print message and make sure there is only one \n at the end of it
	if(LOG_LEVEL_PARTIAL == (consoleLogLevel & LOG_LEVEL_PARTIAL))
		text[text.size() - 1] != '\n' ? Print(message) : Print(message + "\n");

	if(logType != LOG_TYPE_NONE)
		message += std::string(separatorString.begin(), separatorString.end());

	message += text;

	#ifdef __linux__
	if(printStackTrace
			&& (logType == LOG_TYPE_WARNING
					  || logType == LOG_TYPE_ERROR))
	{
		message += "Stack trace:\n";

		void* stackTrace[16];
		int size = backtrace(stackTrace, 16);

		char** strings = backtrace_symbols(stackTrace, size);
		for(int i = 0; i < size; i++)
		{
			int begin = 0;
			while(strings[i][begin] != '(' && strings[i][begin] != ' '
											  && strings[i][begin] != 0)
				++begin;

			char syscom[256];
			char fileBuffer[512]; //Should be big enough for file paths

			sprintf(syscom, "addr2line %p -e %.*s", stackTrace[i], begin, strings[i]);

			FILE* filePtr;
			filePtr = popen(syscom, "r");
			while(fgets(fileBuffer, sizeof(fileBuffer), filePtr) != NULL)
				message += fileBuffer;
			pclose(filePtr);
		}

		free(strings);
	}
	#endif

	if(LOG_LEVEL_FULL == (consoleLogLevel & LOG_LEVEL_FULL)
		|| LOG_LEVEL_EXCLUSIVE == (consoleLogLevel & LOG_LEVEL_EXCLUSIVE))
	{
		Print(message);

		if(LOG_LEVEL_EXCLUSIVE == (consoleLogLevel & LOG_LEVEL_EXCLUSIVE))
			return; //Don't write anything to file
	}

	std::ofstream out;
	out.open(outPath + outName, openMode);

	if(out.is_open())
	{
		out.write(&message[0], message.size());
		out.close();
	}
}

void DLib::Logger::Log(LOG_TYPE logType, const char* text)
{
	Log(logType, std::string(text));
}

void DLib::Logger::ClearLog()
{
	std::ifstream in(outPath + outName);
	if(!in.is_open())
		return; //File didn't exist

	if(in.peek() == std::ifstream::traits_type::eof())
	{
		in.close();
		return; //File is empty
	}

	in.close();

	std::ofstream out;
	out.open(outPath + outName, std::ios_base::trunc | std::ios_base::out);
	out.close();
}

//////////////////////////////////////////////////////////////////////////
//GETTERS
//////////////////////////////////////////////////////////////////////////

void DLib::Logger::SetSeparatorString(const std::string& newSeparatorString)
{
	separatorString = newSeparatorString;
}

void DLib::Logger::SetOutputDir(const std::string& path, std::ios_base::openmode newOpenMode)
{
	outPath = path;
	openMode = newOpenMode;
}

void DLib::Logger::SetOutputName(const std::string& name, std::ios_base::openmode newOpenMode)
{
	outName = name;
	openMode = newOpenMode;
}

void DLib::Logger::SetConsoleLogLevel(CONSOLE_LOG_LEVEL logLevel)
{
	consoleLogLevel = logLevel;
}

void DLib::Logger::Print(std::string message)
{
#ifdef _WIN32
    if(LOG_LEVEL_OUTPUT_DEBUG_STRING == (consoleLogLevel & LOG_LEVEL_OUTPUT_DEBUG_STRING))
		OutputDebugStringA(message.c_str());
    else
        std::cout << message;
#else
    std::cout << message;
#endif
}

#ifdef __linux__
void DLib::Logger::PrintStackTrace(bool print)
{
	printStackTrace = print;
}
#endif