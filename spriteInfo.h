#ifndef SpriteInfo_h__
#define SpriteInfo_h__

#include "rect.h"

#include <glm/glm.hpp>

namespace DLib
{
	struct SpriteInfo
	{
		SpriteInfo() {};
		SpriteInfo(DLib::Rect position, DLib::Rect clipRect, glm::vec4 color)
			: position(position)
			, clipRect(clipRect)
			, color(color) {};
		~SpriteInfo() {};

		DLib::Rect position;
		DLib::Rect clipRect;
		glm::vec4  color;
	};
}

#endif // SpriteInfo_h__
