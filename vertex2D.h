#ifndef Vertex2D_h__
#define Vertex2D_h__

#include <glm/glm.hpp>
#include <algorithm>

namespace DLib
{
	class Vertex2D
	{
	public:
		Vertex2D();
		~Vertex2D();

		float position[2]; //8 bytes
		float texCoords[2]; //8 bytes
		unsigned char color[4]; //4 bytes

		//Total of 20 bytes, so add 12 bytes of padding
		int32_t padding[3];
	};

	/*inline bool operator==(const Vertex2D& lhs, const Vertex2D& rhs)
	{
		return lhs.position == rhs.position && lhs.texCoords == rhs.texCoords && lhs.color == rhs.color;
	}

	inline bool operator!=(const Vertex2D& lhs, const Vertex2D& rhs)
	{
		return !(lhs == rhs);
	}*/
}

#endif // Vertex2D_h__