#include "rect.h"

DLib::Rect::Rect()
	: positionMin(0.0f, 0.0f)
	, positionMax(0.0f, 0.0f)
	, size(0.0f, 0.0f)
{
}

DLib::Rect::Rect(float x, float y, float width, float height)
	: positionMin(x, y)
	, size(width, height)
{
	positionMax = positionMin + size;
}

DLib::Rect::Rect(float x, float y, const glm::vec2& size)
	: positionMin(x, y)
	, size(size)
{
	positionMax = positionMin + size;
}

DLib::Rect::Rect(const glm::vec2& position, float width, float height)
	: positionMin(position)
	, size(width, height)
{
	positionMax = position + size;
}

DLib::Rect::Rect(const glm::vec2& position, const glm::vec2& size)
	: positionMin(position)
	, size(size)
{
	positionMax = positionMin + size;
}

DLib::Rect::~Rect(void)
{
}

void DLib::Rect::Set(float x, float y, float width, float height)
{
	SetPos(x, y);
	SetSize(width, height);
}

void DLib::Rect::Set(float x, float y, const glm::vec2& size)
{
	SetPos(x, y);
	SetSize(size);
}

void DLib::Rect::Set(const glm::vec2& position, float width, float height)
{
	SetPos(position);
	SetSize(width, height);
}

void DLib::Rect::Set(const glm::vec2& position, const glm::vec2& size)
{
	SetPos(position);
	SetSize(size);
}

void DLib::Rect::SetPos(float x, float y)
{
	positionMin.x = x;
	positionMin.y = y;

	positionMax = positionMin + size;
}

void DLib::Rect::SetPos(const glm::vec2& position)
{
	positionMin = position;
	positionMax = position + size;
}

void DLib::Rect::SetSize(float width, float height)
{
	size.x = width;
	size.y = height;

	positionMax = positionMin + size;
}

void DLib::Rect::SetSize(const glm::vec2& size)
{
	this->size = size;

	positionMax = positionMin + size;
}

bool DLib::Rect::Contains(float x, float y) const
{
	//Intentionally left as x < positionMax.x to make it zero-indexed
	return (x >= positionMin.x
		&& x < positionMax.x
		&& y >= positionMin.y
		&& y < positionMax.y);
}

bool DLib::Rect::Contains(const glm::vec2& position) const
{
	//Intentionally left as x < positionMax.x to make it zero-indexed
	return (position.x >= positionMin.x
		&& position.x < positionMax.x
		&& position.y >= positionMin.y
		&& position.y < positionMax.y);
}

glm::vec2 DLib::Rect::GetMinPosition() const
{
	return positionMin;
}

glm::vec2 DLib::Rect::GetMaxPosition() const
{
	return positionMax;
}

glm::vec2 DLib::Rect::GetSize() const
{
	return size;
}

float DLib::Rect::GetWidth() const
{
	return size.x;
}

float DLib::Rect::GetHeight() const
{
	return size.y;
}

DLib::Rect& DLib::Rect::operator+=(const Rect& rhs)
{
	positionMin += rhs.GetMinPosition();
	positionMax += rhs.GetMaxPosition();

	size = positionMax - positionMin;

	return *this;
}

DLib::Rect& DLib::Rect::operator-=(const Rect& rhs)
{
	positionMin -= rhs.GetMinPosition();
	positionMax -= rhs.GetMaxPosition();

	size = positionMax - positionMin;

	return *this;
}

DLib::Rect& DLib::Rect::operator*=(const Rect& rhs)
{
	positionMin *= rhs.GetMinPosition();
	positionMax *= rhs.GetMaxPosition();

	size = positionMax - positionMin;

	return *this;
}

DLib::Rect& DLib::Rect::operator/=(const Rect& rhs)
{
	positionMin /= rhs.GetMinPosition();
	positionMax /= rhs.GetMaxPosition();

	size = positionMax - positionMin;

	return *this;
}

DLib::Rect& DLib::Rect::operator+=(const glm::vec2& rhs)
{
	positionMin += rhs;
	positionMax = positionMin + size;

	return *this;
}

DLib::Rect& DLib::Rect::operator-=(const glm::vec2& rhs)
{
	positionMin -= rhs;
	positionMax = positionMin + size;

	return *this;
}

DLib::Rect& DLib::Rect::operator/=(const glm::vec2& rhs)
{
	positionMin /= rhs;
	positionMax = positionMin + size;

	return *this;
}

DLib::Rect& DLib::Rect::operator*=(const glm::vec2& rhs)
{
	positionMin /= rhs;
	positionMax = positionMin + size;

	return *this;
}

const DLib::Rect DLib::Rect::empty(0.0f, 0.0f, 0.0f, 0.0f);