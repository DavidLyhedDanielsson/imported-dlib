#include "keyState.h"

DLib::KeyState::KeyState()
{
}

DLib::KeyState::KeyState(int key, int action, int mods)
	: key(key)
	, action(action)
	, mods(mods)
{
}

DLib::KeyState::~KeyState()
{
}

const DLib::KeyState DLib::KeyState::AltF1(GLFW_KEY_F1, 0, GLFW_MOD_ALT);
const DLib::KeyState DLib::KeyState::AltF2(GLFW_KEY_F2, 0, GLFW_MOD_ALT);
const DLib::KeyState DLib::KeyState::AltF3(GLFW_KEY_F3, 0, GLFW_MOD_ALT);
const DLib::KeyState DLib::KeyState::AltF4(GLFW_KEY_F4, 0, GLFW_MOD_ALT);
const DLib::KeyState DLib::KeyState::AltF5(GLFW_KEY_F5, 0, GLFW_MOD_ALT);
const DLib::KeyState DLib::KeyState::AltF6(GLFW_KEY_F6, 0, GLFW_MOD_ALT);
const DLib::KeyState DLib::KeyState::AltF7(GLFW_KEY_F7, 0, GLFW_MOD_ALT);
const DLib::KeyState DLib::KeyState::AltF8(GLFW_KEY_F8, 0, GLFW_MOD_ALT);
const DLib::KeyState DLib::KeyState::AltF9(GLFW_KEY_F9, 0, GLFW_MOD_ALT);
const DLib::KeyState DLib::KeyState::AltF10(GLFW_KEY_F10, 0, GLFW_MOD_ALT);
const DLib::KeyState DLib::KeyState::AltF11(GLFW_KEY_F11, 0, GLFW_MOD_ALT);
const DLib::KeyState DLib::KeyState::AltF12(GLFW_KEY_F12, 0, GLFW_MOD_ALT);

const DLib::KeyState DLib::KeyState::CtrlF1(GLFW_KEY_F1, 0, GLFW_MOD_CONTROL);
const DLib::KeyState DLib::KeyState::CtrlF2(GLFW_KEY_F2, 0, GLFW_MOD_CONTROL);
const DLib::KeyState DLib::KeyState::CtrlF3(GLFW_KEY_F3, 0, GLFW_MOD_CONTROL);
const DLib::KeyState DLib::KeyState::CtrlF4(GLFW_KEY_F4, 0, GLFW_MOD_CONTROL);
const DLib::KeyState DLib::KeyState::CtrlF5(GLFW_KEY_F5, 0, GLFW_MOD_CONTROL);
const DLib::KeyState DLib::KeyState::CtrlF6(GLFW_KEY_F6, 0, GLFW_MOD_CONTROL);
const DLib::KeyState DLib::KeyState::CtrlF7(GLFW_KEY_F7, 0, GLFW_MOD_CONTROL);
const DLib::KeyState DLib::KeyState::CtrlF8(GLFW_KEY_F8, 0, GLFW_MOD_CONTROL);
const DLib::KeyState DLib::KeyState::CtrlF9(GLFW_KEY_F9, 0, GLFW_MOD_CONTROL);
const DLib::KeyState DLib::KeyState::CtrlF10(GLFW_KEY_F10, 0, GLFW_MOD_CONTROL);
const DLib::KeyState DLib::KeyState::CtrlF11(GLFW_KEY_F11, 0, GLFW_MOD_CONTROL);
const DLib::KeyState DLib::KeyState::CtrlF12(GLFW_KEY_F12, 0, GLFW_MOD_CONTROL);

const DLib::KeyState DLib::KeyState::ShiftF1(GLFW_KEY_F1, 0, GLFW_MOD_SHIFT);
const DLib::KeyState DLib::KeyState::ShiftF2(GLFW_KEY_F2, 0, GLFW_MOD_SHIFT);
const DLib::KeyState DLib::KeyState::ShiftF3(GLFW_KEY_F3, 0, GLFW_MOD_SHIFT);
const DLib::KeyState DLib::KeyState::ShiftF4(GLFW_KEY_F4, 0, GLFW_MOD_SHIFT);
const DLib::KeyState DLib::KeyState::ShiftF5(GLFW_KEY_F5, 0, GLFW_MOD_SHIFT);
const DLib::KeyState DLib::KeyState::ShiftF6(GLFW_KEY_F6, 0, GLFW_MOD_SHIFT);
const DLib::KeyState DLib::KeyState::ShiftF7(GLFW_KEY_F7, 0, GLFW_MOD_SHIFT);
const DLib::KeyState DLib::KeyState::ShiftF8(GLFW_KEY_F8, 0, GLFW_MOD_SHIFT);
const DLib::KeyState DLib::KeyState::ShiftF9(GLFW_KEY_F9, 0, GLFW_MOD_SHIFT);
const DLib::KeyState DLib::KeyState::ShiftF10(GLFW_KEY_F10, 0, GLFW_MOD_SHIFT);
const DLib::KeyState DLib::KeyState::ShiftF11(GLFW_KEY_F11, 0, GLFW_MOD_SHIFT);
const DLib::KeyState DLib::KeyState::ShiftF12(GLFW_KEY_F12, 0, GLFW_MOD_SHIFT);