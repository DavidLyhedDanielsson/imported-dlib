#ifndef VertexShader_h__
#define VertexShader_h__

#include "content.h"

#include <GL/glew.h>

namespace DLib
{
	class VertexShader :
		public Content
	{
	public:
		VertexShader();
		~VertexShader();

		GLuint GetShader() const;

	private:
        bool Load(const std::string& path, DLib::ContentManager* contentManager = nullptr, ContentParameters* contentParameters = nullptr);
        void Unload(DLib::ContentManager* contentManager = nullptr);

		GLuint shader;
	};
}

#endif // VertexShader_h__
