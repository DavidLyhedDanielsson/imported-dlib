#ifndef CharacterSet_h__
#define CharacterSet_h__

#include "character.h"

#include <string>
#include <vector>
#include <unordered_map>

#include "constructedString.h"
#include "texture2D.h"
#include "content.h"
#include "xmlFile.h"

namespace DLib
{
	class CharacterSet : public DLib::Content
	{
	public:
		CharacterSet();
		//CharacterSet(const DLib::CharacterSet& rhs);
		//CharacterSet(const DLib::CharacterSet&& rhs);
		~CharacterSet();

		//TODO: Forbid copying? It's probably a terrible idea
		CharacterSet& operator=(const DLib::CharacterSet& rhs) = delete;
		CharacterSet& operator=(DLib::CharacterSet&& rhs) = delete;

		const static int SPACE_CHARACTER = ' '; //Change this if needed. Should correspond to your desired value for a blankspace

		std::string GetName() const;
		unsigned int GetFontSize() const;
		Texture2D* GetTexture() const;

		int GetSpaceXAdvance() const;

		const Character* GetCharacter(unsigned int id) const;
		DLib::ConstructedString ConstructString(const char* text) const;
		DLib::ConstructedString ConstructString(const std::string& text) const;
		//************************************
		// Method:		InsertCharacter
		// FullName:	DLib::CharacterSet::InsertCharacter
		// Access:		public 
		// Returns:		void
		// Qualifier:	const
		// Parameter:	DLib::ConstructedString& constructedString
		// Parameter:	int index
		// Parameter:	unsigned int character
		// Description:	Inserts character at the index into constructedString
		//************************************
        void Insert(DLib::ConstructedString& constructedString, unsigned int index, const std::string& string) const;
		void Insert(DLib::ConstructedString& constructedString, int index, unsigned int character) const;
		//************************************
		// Method:		RemoveCharacters
		// FullName:	DLib::CharacterSet::RemoveCharacters
		// Access:		public 
		// Returns:		void
		// Qualifier:	const
		// Parameter:	DLib::ConstructedString & constructedString
		// Parameter:	int startIndex
		// Parameter:	int count
		// Description:	Removes characters from constructedString starting at startIndex and continuing forwards
		//************************************
		void Erase(DLib::ConstructedString& constructedString, unsigned int startIndex, unsigned int count) const;

		//************************************
		// Method:		GetLineHeight
		// FullName:	DLib::CharacterSet::GetLineHeight
		// Access:		public 
		// Returns:		unsigned int
		// Qualifier:	const
		// Description:	Returns the line height for the currently loaded font
		//************************************
		unsigned int GetLineHeight() const;
		//************************************
		// Method:		GetWidthAtMaxWidth
		// FullName:	DLib::CharacterSet::GetWidthAtMaxWidth
		// Access:		public 
		// Returns:		unsigned int - width
		// Qualifier:	const
		// Parameter:	const DLib::ConstructedString& constructedString
		// Parameter:	unsigned int maxWidth - width to measure to
		// Description:	Returns the (graphical) width of constructedString by counting characters until maxWidth is reached or would be exceeded at the next character
		//************************************
		unsigned int GetWidthAtMaxWidth(const DLib::ConstructedString& constructedString, unsigned int maxWidth) const;
		//************************************
		// Method:		GetWidthAtIndex
		// FullName:	DLib::CharacterSet::GetWidthAtIndex
		// Access:		public 
		// Returns:		unsigned int - width
		// Qualifier:	const
		// Parameter:	const DLib::ConstructedString& constructedString
		// Parameter:	unsigned int index - character index to measure to
		// Description:	Returns the (graphical) width of constructedString from the start to index
		//************************************
		unsigned int GetWidthAtIndex(const DLib::ConstructedString& constructedString, int index) const;

		//************************************
		// Method:		GetIndexAtWidth
		// FullName:	DLib::CharacterSet::GetIndexAtWidth
		// Access:		public 
		// Returns:		unsigned int - index
		// Qualifier:	const
		// Parameter:	const DLib::ConstructedString& constructedString
		// Parameter:	int width - width to measure to
		// Parameter:	float roundingValue - index rouning value. If this value is 0.5 and the user clicks in the middle of the character the method will return the index after the character. If this value is 0.55 and the user clicks in the middle the method will return the index of the current character
		// Description:	
		//************************************
		unsigned int GetIndexAtWidth(const DLib::ConstructedString& constructedString, int width, float roundingValue = 0.6f) const;

		//************************************
		// Method:		GetRowsAtWidth
		// Returns:		unsigned int
		// Parameter:	const ConstructedString& constructedString
		// Parameter:	int width - width to measure to
		// Description:	Returns the amount of rows the given string will have at the given width
		//************************************
		unsigned int GetRowsAtWidth(const DLib::ConstructedString& constructedString, unsigned int width) const;
	private:
        const unsigned int errorCharacterID = 0x3F; //0x3F = "?"

		void XMLSubscriber(const XMLElement& element);
		//char GetKerningOffset(char first, char second) const; //TODO: Save kerning pairs in this class

        bool Load(const std::string& path, DLib::ContentManager* contentManager = nullptr, ContentParameters* contentParameters = nullptr);
        void Unload(DLib::ContentManager* contentManager = nullptr);

		std::string name;
		std::unordered_map<unsigned int, DLib::Character> characters;

		unsigned int fontSize;
		unsigned int lineHeight;
		unsigned int spaceXAdvance;

		DLib::Texture2D* texture;
	};
}

#endif // CharacterSet_h__
