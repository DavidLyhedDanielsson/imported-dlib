#include "axisAlignedBoundingBox.h"

DLib::AxisAlignedBoundingBox::AxisAlignedBoundingBox()
{
	minPoint = glm::vec3(0.0f, 0.0f, 0.0f);
	maxPoint = glm::vec3(0.0f, 0.0f, 0.0f);
}

DLib::AxisAlignedBoundingBox::AxisAlignedBoundingBox(glm::vec3 minPoint, glm::vec3 maxPoint)
{
	this->minPoint = minPoint;
	this->maxPoint = maxPoint;
}

DLib::AxisAlignedBoundingBox::~AxisAlignedBoundingBox()
{
}

bool DLib::AxisAlignedBoundingBox::Contains(glm::vec3 point) const
{
	return glm::all(glm::greaterThanEqual(point, minPoint)) && glm::all(glm::lessThanEqual(point, maxPoint));
	//return DirectX::XMVector3GreaterOrEqual(testPoint, thisMin) && DirectX::XMVector3LessOrEqual(testPoint, thisMax);
}

bool DLib::AxisAlignedBoundingBox::Contains(DLib::AxisAlignedBoundingBox box) const
{
	return glm::all(glm::greaterThanEqual(box.GetMinPoint(), minPoint)) && glm::all(glm::lessThanEqual(box.GetMaxPoint(), maxPoint));
	//return DirectX::XMVector3GreaterOrEqual(boxMin, thisMin) && DirectX::XMVector3LessOrEqual(boxMax, thisMax);
}

bool DLib::AxisAlignedBoundingBox::Intersects(DLib::AxisAlignedBoundingBox box) const
{
// 	glm::vec3 thisMax = DirectX::XMLoadFloat3(&maxPoint);
// 	glm::vec3 boxMin = DirectX::XMLoadFloat3(&box.GetMinPoint());

	if(glm::any(glm::greaterThanEqual(box.GetMinPoint(), minPoint)))
		return false;

// 	if(DirectX::XMComparisonAnyTrue(DirectX::XMVector3GreaterOrEqualR(boxMin, thisMax)))
// 		return false;

	//if(box.minPoint.x > maxPoint.x || box.minPoint.y > maxPoint.y || box.minPoint.z > maxPoint.z)
	//	return false;

// 	glm::vec3 thisMin = DirectX::XMLoadFloat3(&minPoint);
// 	glm::vec3 boxMax = DirectX::XMLoadFloat3(&box.GetMaxPoint());

	if(!glm::all(glm::greaterThanEqual(box.GetMaxPoint(), maxPoint)))
		return false;

// 	if(DirectX::XMComparisonAnyFalse(DirectX::XMVector3GreaterOrEqualR(boxMax, thisMin)))
// 		return false;

	return true;

	//if(box.maxPoint.x < minPoint.x || box.maxPoint.y < minPoint.y || box.maxPoint.z < minPoint.z)
	//	return false;

	//return true;
}

void DLib::AxisAlignedBoundingBox::Move(glm::vec3 amount)
{
	minPoint += amount;
	maxPoint += amount;
}

glm::vec3 DLib::AxisAlignedBoundingBox::GetMinPoint() const
{
	return minPoint;
}

glm::vec3 DLib::AxisAlignedBoundingBox::GetMaxPoint() const
{
	return maxPoint;
}

glm::vec3 DLib::AxisAlignedBoundingBox::GetMidPoint() const
{
	return glm::vec3((minPoint.x + minPoint.x) * 0.5f, (minPoint.y + minPoint.y) * 0.5f, (minPoint.z + minPoint.z) * 0.5f);
}

void DLib::AxisAlignedBoundingBox::GetCorners(glm::vec3 (&out)[8]) const
{
	glm::vec3 length = glm::vec3(maxPoint.x - minPoint.x, maxPoint.y - minPoint.y, maxPoint.z - minPoint.z);

	//min
	out[0] = minPoint;
	out[1] = glm::vec3(minPoint.x + length.x, minPoint.y, minPoint.z);
	out[2] = glm::vec3(minPoint.x, minPoint.y + length.y, minPoint.z);
	out[3] = glm::vec3(minPoint.x + length.x, minPoint.y + length.y, minPoint.z);

	//max
	out[4] = maxPoint;
	out[5] = glm::vec3(maxPoint.x - length.x, maxPoint.y, maxPoint.z);
	out[6] = glm::vec3(maxPoint.x, maxPoint.y - length.y, maxPoint.z);
	out[7] = glm::vec3(maxPoint.x - length.x, maxPoint.y - length.y, maxPoint.z);
}

bool DLib::AxisAlignedBoundingBox::operator==(const DLib::AxisAlignedBoundingBox& box) const
{
	return(minPoint.x == box.minPoint.x && minPoint.y == box.minPoint.y && minPoint.z == box.minPoint.z &&
		   maxPoint.x == box.maxPoint.x && maxPoint.y == box.maxPoint.y && maxPoint.z == box.maxPoint.z);
}

bool DLib::AxisAlignedBoundingBox::operator!=(const DLib::AxisAlignedBoundingBox& box) const
{
	return(minPoint.x != box.minPoint.x || minPoint.y != box.minPoint.y || minPoint.z != box.minPoint.z ||
		   maxPoint.x != box.maxPoint.x || maxPoint.y != box.maxPoint.y || maxPoint.z != box.maxPoint.z);
}

bool DLib::AxisAlignedBoundingBox::operator>(const DLib::AxisAlignedBoundingBox& box) const
{
	return(minPoint.x > box.minPoint.x && minPoint.y > box.minPoint.y && minPoint.z > box.minPoint.z &&
		   maxPoint.x > box.maxPoint.x && maxPoint.y > box.maxPoint.y && maxPoint.z > box.maxPoint.z);
}

bool DLib::AxisAlignedBoundingBox::operator>=(const DLib::AxisAlignedBoundingBox& box) const
{
	return(minPoint.x >= box.minPoint.x && minPoint.y >= box.minPoint.y && minPoint.z >= box.minPoint.z &&
		   maxPoint.x >= box.maxPoint.x && maxPoint.y >= box.maxPoint.y && maxPoint.z >= box.maxPoint.z);
}

bool DLib::AxisAlignedBoundingBox::operator<(const DLib::AxisAlignedBoundingBox& box) const
{
	return(minPoint.x < box.minPoint.x && minPoint.y < box.minPoint.y && minPoint.z < box.minPoint.z &&
		   maxPoint.x < box.maxPoint.x && maxPoint.y < box.maxPoint.y && maxPoint.z < box.maxPoint.z);
}

bool DLib::AxisAlignedBoundingBox::operator<=(const DLib::AxisAlignedBoundingBox& box) const
{
	return(minPoint.x <= box.minPoint.x && minPoint.y <= box.minPoint.y && minPoint.z <= box.minPoint.z &&
		   maxPoint.x <= box.maxPoint.x && maxPoint.y <= box.maxPoint.y && maxPoint.z <= box.maxPoint.z);
}
