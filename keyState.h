#ifndef KeyState_h__
#define KeyState_h__

#include <GLFW/glfw3.h>

namespace DLib
{
	//Container for GLFW key events
	class KeyState
	{
	public:
		KeyState();
		KeyState(int key, int action, int mods);
		~KeyState();

		int key;
		int action;
		int mods;

		static const KeyState AltF1;
		static const KeyState AltF2;
		static const KeyState AltF3;
		static const KeyState AltF4;
		static const KeyState AltF5;
		static const KeyState AltF6;
		static const KeyState AltF7;
		static const KeyState AltF8;
		static const KeyState AltF9;
		static const KeyState AltF10;
		static const KeyState AltF11;
		static const KeyState AltF12;

		static const KeyState CtrlF1;
		static const KeyState CtrlF2;
		static const KeyState CtrlF3;
		static const KeyState CtrlF4;
		static const KeyState CtrlF5;
		static const KeyState CtrlF6;
		static const KeyState CtrlF7;
		static const KeyState CtrlF8;
		static const KeyState CtrlF9;
		static const KeyState CtrlF10;
		static const KeyState CtrlF11;
		static const KeyState CtrlF12;

		static const KeyState ShiftF1;
		static const KeyState ShiftF2;
		static const KeyState ShiftF3;
		static const KeyState ShiftF4;
		static const KeyState ShiftF5;
		static const KeyState ShiftF6;
		static const KeyState ShiftF7;
		static const KeyState ShiftF8;
		static const KeyState ShiftF9;
		static const KeyState ShiftF10;
		static const KeyState ShiftF11;
		static const KeyState ShiftF12;
	};
}

inline bool operator==(const DLib::KeyState& lhs, const DLib::KeyState& rhs)
{
	//GLFW_RELEASE = 0
	//Custom "not pressed" = -1
	//Make sure keys and mods match and that they are both pressed (or clicked)
	return lhs.key == rhs.key && lhs.mods == rhs.mods && lhs.action > 0 && rhs.action > 0;
}

inline bool operator!=(const DLib::KeyState& lhs, const DLib::KeyState& rhs)
{
	return !operator==(lhs, rhs);
}

#endif // KeyState_h__