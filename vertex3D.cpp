#include "vertex3D.h"

DLib::Vertex3D::Vertex3D()
{
	position = glm::vec3(0.0f, 0.0f, 0.0f);
	normal = glm::vec3(0.0f, 0.0f, 0.0f);
	texCoords = glm::vec2(0.0f, 0.0f);
}

DLib::Vertex3D::~Vertex3D()
{

}

bool DLib::Vertex3D::operator==(Vertex3D rhs) const
{
	return (position.x == rhs.position.x && position.y == rhs.position.y && position.z == rhs.position.z &&
		normal.x == rhs.normal.x && normal.y == rhs.normal.y && normal.z == rhs.normal.z &&
		texCoords.x == rhs.texCoords.x && texCoords.y == rhs.texCoords.y);
}

bool DLib::Vertex3D::operator!=(Vertex3D rhs) const
{
	return (position.x != rhs.position.x || position.y != rhs.position.y || position.z != rhs.position.z ||
		normal.x != rhs.normal.x || normal.y != rhs.normal.y || normal.z != rhs.normal.z ||
		texCoords.x != rhs.texCoords.x || texCoords.y != rhs.texCoords.y);
}