#include "spriteRenderer.h"
#include <stddef.h>
#include <fstream>
#include <iostream>

#include <assert.h>

#include "texture2DCreateParameters.h"

#include "logger.h"

#include "SOIL2.h"

#include <glm/gtc/matrix_transform.hpp>

using std::string;

DLib::Texture2D* DLib::SpriteRenderer::whiteTexture;

DLib::SpriteRenderer::SpriteRenderer()
	: vertexBufferArrayInserts(0)
	, bufferInserts(0)
	, vertexDataStreamOffset(0)
	, indexDataStreamOffset(0)
	, drawOffset(0)
{
}

DLib::SpriteRenderer::~SpriteRenderer()
{

}

void DLib::SpriteRenderer::Init(DLib::ContentManager* contentManager, int xRes, int yRes)
{
	this->contentManager = contentManager;

	vertexBufferArrayInserts = 0;

	SetResolution(xRes, yRes);

	hasBegun = false;

	Texture2DCreateParameters whiteTextureParameters;
	whiteTextureParameters.texParams.minFiler = GL_NEAREST;
	whiteTextureParameters.texParams.magFiler = GL_NEAREST;
	whiteTextureParameters.texParams.wrapR = GL_CLAMP_TO_EDGE;
	whiteTextureParameters.texParams.wrapS = GL_CLAMP_TO_EDGE;
	whiteTextureParameters.texParams.wrapT = GL_CLAMP_TO_EDGE;

	//RGB RGB
	//RGB RGB
	unsigned char whiteTextureData[] =
	{ 
		255, 255, 255, 255, 255, 255,
		255, 255, 255, 255, 255, 255
	};

	whiteTextureParameters.channels = 3;
	whiteTextureParameters.data = &whiteTextureData[0];
	whiteTextureParameters.width = 2;
	whiteTextureParameters.height = 2;
	whiteTextureParameters.uniqueID = "WhiteTexture";

	whiteTexture = contentManager->Load<Texture2D>("", &whiteTextureParameters);

	////////////////////////////////////////////////////////////
	//CREATE BUFFERS
	////////////////////////////////////////////////////////////
	glGenBuffers(1, &vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, MAX_VERTEX_BUFFER_SIZE, 0, GL_STREAM_DRAW);

	glGenBuffers(1, &elementBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, MAX_ELEMENT_BUFFER_SIZE, 0, GL_STREAM_DRAW);

	//////////////////////////////////////////////////////////////////////////
	//SHADERS
	//////////////////////////////////////////////////////////////////////////
	//Position
	shaderProgram.AddVertexAttrib(GL_FLOAT, 2, 32);
	//TexCoords
	shaderProgram.AddVertexAttrib(GL_FLOAT, 2, 32);
	//Color
	shaderProgram.AddVertexAttrib(GL_UNSIGNED_BYTE, 4, 32);

	shaderProgram.LoadProgram(contentManager, "SpriteRendererVertexShader.glsl", "SpriteRendererFragmentShader.glsl");
	shaderProgram.Unbind();

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void DLib::SpriteRenderer::Begin()
{
	if(hasBegun)
		Logger::LogLine(DLib::LOG_TYPE_WARNING, "SpriteRenderer::Begin called twice in a row!");

	shaderProgram.Bind();
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBuffer);
	glEnable(GL_BLEND);
	glBlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	bufferInserts = 0;
	currentTexture = 0;

	hasBegun = true;
}

void DLib::SpriteRenderer::End()
{
	if(!hasBegun)
		Logger::LogLine(DLib::LOG_TYPE_WARNING, "SpriteRenderer::End called without begin");

	if(spriteBatch.size() > 0)
		Draw();

	shaderProgram.Unbind();

	glDisable(GL_BLEND);

	hasBegun = false;
}

//void DLib::SpriteRenderer::DrawString(const Texture2D& texture2D, const glm::vec2& position, const ConstructedString& characters, const glm::vec4& color /*= glm::vec4(1.0f, 1.0f, 1.0f, 1.0f)*/, unsigned int maxRowLength /*= 0xFFFFFFFF*/)
//{
	//int xAdvance = 0;
	//int yAdvance = 0;

	//Character currentChar;

	//if(characters.GetLength() > maxRowLength)
	//{
	//	int currentRow = 0;
	//	int currentWord = 0;

	//	unsigned int lastSpace = 0;

	//	for(unsigned int i = 0; i < characters.Size(); i++)
	//	{
	//		if(characters[i].id == 32)
	//		{
	//			lastSpace = i;
	//			currentWord = 0;
	//		}

	//		currentRow += characters[i].xAdvance;
	//		currentWord += characters[i].xAdvance;

	//		if(currentRow > (int)maxRowLength)
	//		{
	//			currentWord -= characters[i].xAdvance;

	//			Character newCharacter = characters[lastSpace];

	//			newCharacter.id = '\n';
	//			newCharacter.xAdvance = 0;

	//			//characters.ChangeCharacterAt(lastSpace, newCharacter);

	//			currentRow = currentWord;
	//		}
	//	}
	//}

	//for (unsigned int i = 0; i < characters.Size(); i++)
	//{
	//	currentChar = characters[i];

	//	if(currentChar.id == '\n')
	//	{
	//		xAdvance = 0;
	//		yAdvance += currentChar.height; //All chars have the same height (font size)
	//	}
	//	else if(currentChar.id != 32)  //space
	//		Draw(texture2D, DLib::Rect(position.x + xAdvance + currentChar.xOffset, position.y + yAdvance, currentChar.width, currentChar.height), DLib::Rect(currentChar.x, currentChar.y, currentChar.width, currentChar.height), color);

	//	xAdvance += currentChar.xAdvance;
	//}
//}

void DLib::SpriteRenderer::Draw(const DLib::Texture2D& texture2D, const DLib::Rect& drawRect, const glm::vec4& color)
{
    Draw(texture2D, drawRect, DLib::Rect(0.0f, 0.0f, texture2D.GetSize()), color);
}

void DLib::SpriteRenderer::Draw(const DLib::Texture2D& texture2D, const glm::vec2& position, const DLib::Rect& clipRect, const glm::vec4& color)
{
	if(currentTexture == texture2D.GetTexture())
	{
		spriteBatch.back().batchData.emplace_back(
				position, position + clipRect.GetSize(), clipRect.GetMinPosition() * texture2D.GetPredivSize(), clipRect.GetMaxPosition() * texture2D.GetPredivSize(), color);
	}
	else
	{
		currentTexture = texture2D.GetTexture();
		spriteBatch.emplace_back(texture2D.GetTexture(), BatchData(
				position, position + clipRect.GetSize(), clipRect.GetMinPosition() * texture2D.GetPredivSize(), clipRect.GetMaxPosition() * texture2D.GetPredivSize(), color));
	}

	bufferInserts++;
	if(bufferInserts == MAX_BUFFER_INSERTS)
		Draw();
}

void DLib::SpriteRenderer::Draw(const DLib::Texture2D& texture2D, const DLib::Rect& position, const DLib::Rect& clipRect, const glm::vec4& color)
{
	if(currentTexture == texture2D.GetTexture())
	{
		spriteBatch.back().batchData.emplace_back(
				position.GetMinPosition(), position.GetMaxPosition(), clipRect.GetMinPosition() * texture2D.GetPredivSize(), clipRect.GetMaxPosition() * texture2D.GetPredivSize(), color);
	}
	else
	{
		currentTexture = texture2D.GetTexture();
		spriteBatch.emplace_back(texture2D.GetTexture(), BatchData(
				position.GetMinPosition(), position.GetMaxPosition(), clipRect.GetMinPosition() * texture2D.GetPredivSize(), clipRect.GetMaxPosition() * texture2D.GetPredivSize(), color));
	}

	bufferInserts++;
	if(bufferInserts == MAX_BUFFER_INSERTS)
		Draw();
}

void DLib::SpriteRenderer::Draw(const DLib::Rect& drawRect, const glm::vec4& color /*= glm::vec4(1.0f, 1.0f, 1.0f, 1.0f)*/)
{
	if(currentTexture == whiteTexture->GetTexture())
	{
		spriteBatch.back().batchData.emplace_back(
				drawRect.GetMinPosition(), drawRect.GetMaxPosition(), glm::vec2(0.0f, 0.0f), glm::vec2(1.0f, 1.0f), color);
	}
	else
	{
		currentTexture = whiteTexture->GetTexture();
		spriteBatch.emplace_back(whiteTexture->GetTexture(), BatchData(
						drawRect.GetMinPosition(), drawRect.GetMaxPosition(), glm::vec2(0.0f, 0.0f), glm::vec2(1.0f, 1.0f), color));
	}

	bufferInserts++;
	if(bufferInserts == MAX_BUFFER_INSERTS)
		Draw();
}

void DLib::SpriteRenderer::Draw()
{
	//////////////////////////////////////////////////////////////////////////
	//MAP/ORPHAN VERTEX BUFFER
	//////////////////////////////////////////////////////////////////////////
	GLuint streamVertexDataSize = 0; //static_cast<GLuint>(spriteBatch.size()) * static_cast<unsigned int>(sizeof(Vertex2D) * 4);
	GLuint streamIndexDataSize = 0; //static_cast<GLuint>(spriteBatch.size()) * static_cast<unsigned int>(sizeof(GLuint) * 6);

	for(const SpriteBatch& batch : spriteBatch)
	{
		streamVertexDataSize += static_cast<GLuint>(batch.batchData.size()) * static_cast<unsigned int>(sizeof(Vertex2D) * 4);
		streamIndexDataSize += static_cast<GLuint>(batch.batchData.size()) * static_cast<unsigned int>(sizeof(GLuint) * 6);
	}

	//Orphaning
	if(vertexDataStreamOffset + streamVertexDataSize > MAX_VERTEX_BUFFER_SIZE)
	{
		//If one needs to be orphaned then both do
		glBufferData(GL_ARRAY_BUFFER, MAX_VERTEX_BUFFER_SIZE, 0, GL_STREAM_DRAW);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, MAX_ELEMENT_BUFFER_SIZE, 0, GL_STREAM_DRAW);

		indexDataStreamOffset = 0;
		vertexDataStreamOffset = 0;
		drawOffset = 0;
		vertexBufferArrayInserts = 0;
	}

	//////////////////////////////////////////////////////////////////////////
	//MAP
	//////////////////////////////////////////////////////////////////////////
	Vertex2D* mappedVertexData = static_cast<Vertex2D*>(glMapBufferRange(GL_ARRAY_BUFFER, vertexDataStreamOffset, streamVertexDataSize, GL_MAP_WRITE_BIT | GL_MAP_UNSYNCHRONIZED_BIT));
	vertexDataStreamOffset += streamVertexDataSize;

	GLuint* mappedIndexData = static_cast<GLuint*>(glMapBufferRange(GL_ELEMENT_ARRAY_BUFFER, indexDataStreamOffset, streamIndexDataSize, GL_MAP_WRITE_BIT | GL_MAP_UNSYNCHRONIZED_BIT));
	indexDataStreamOffset += streamIndexDataSize;

	if(mappedVertexData == 0 || mappedIndexData == 0)
	{
		Logger::LogLine(DLib::LOG_TYPE_ERROR, "Couldn't map vertex/index buffer!");
		assert((mappedVertexData != 0 || mappedIndexData != 0) && "Couldn't map vertex buffer!");
	}

	//Buffers are bound in Begin()
	GLuint currentInserts = 0;
	for(const SpriteBatch& batch : spriteBatch)
	{
		//////////////////////////////////////////////////////////////////////////
		//FILL BUFFER WITH DATA
		//////////////////////////////////////////////////////////////////////////
		for(const BatchData& data : batch.batchData)
		{
			DLib::Vertex2D vertex2D;
			//TOP LEFT
			vertex2D.position[0] = data.positionMin.x;
			vertex2D.position[1] = data.positionMin.y;

			vertex2D.texCoords[0] = data.texCoordsMin.x;
			vertex2D.texCoords[1] = data.texCoordsMin.y;

			//Only needs to be set once
			vertex2D.color[0] = static_cast<unsigned char>(data.color.x * 255.0f);
			vertex2D.color[1] = static_cast<unsigned char>(data.color.y * 255.0f);
			vertex2D.color[2] = static_cast<unsigned char>(data.color.z * 255.0f);
			vertex2D.color[3] = static_cast<unsigned char>(data.color.w * 255.0f);

			mappedVertexData[currentInserts * 4] = vertex2D;

			//TOP RIGHT
			//Only update x-related to max
			vertex2D.position[0] = data.positionMax.x;
			vertex2D.texCoords[0] = data.texCoordsMax.x;
			mappedVertexData[currentInserts * 4 + 1] = vertex2D;

			//BOTTOM RIGHT
			//Only update y-related to max
			vertex2D.position[1] = data.positionMax.y;
			vertex2D.texCoords[1] = data.texCoordsMax.y;
			mappedVertexData[currentInserts * 4 + 2] = vertex2D;

			//BOTTOM LEFT
			//Only update x-related to min
			vertex2D.position[0] = data.positionMin.x;
			vertex2D.texCoords[0] = data.texCoordsMin.x;
			mappedVertexData[currentInserts * 4 + 3] = vertex2D;

			//ELEMENT BUFFER
			mappedIndexData[currentInserts * 6] = vertexBufferArrayInserts * 4;
			mappedIndexData[currentInserts * 6 + 1] = vertexBufferArrayInserts * 4 + 1;
			mappedIndexData[currentInserts * 6 + 2] = vertexBufferArrayInserts * 4 + 2;
			mappedIndexData[currentInserts * 6 + 3] = vertexBufferArrayInserts * 4 + 2;
			mappedIndexData[currentInserts * 6 + 4] = vertexBufferArrayInserts * 4 + 3;
			mappedIndexData[currentInserts * 6 + 5] = vertexBufferArrayInserts * 4;

			currentInserts++;
			vertexBufferArrayInserts++;
		}
	}

	glUnmapBuffer(GL_ARRAY_BUFFER);
	glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);

	for(const SpriteBatch& batch : spriteBatch)
	{
		glBindTexture(GL_TEXTURE_2D, batch.texture);
		glDrawElements(GL_TRIANGLES, static_cast<GLsizei>(batch.batchData.size()) * 6, GL_UNSIGNED_INT, (void*)(drawOffset * 6 * sizeof(GLuint)));
		drawOffset += static_cast<GLsizei>(batch.batchData.size());
	}

	spriteBatch.clear();
	bufferInserts = 0;
	currentTexture = 0;
}

void DLib::SpriteRenderer::Reload()
{
}

void DLib::SpriteRenderer::SetResolution(int xRes, int yRes)
{
	resolutionRect.Set(0, 0, static_cast<float>(xRes), static_cast<float>(yRes));

	shaderProgram.SetProjectionMatrix(glm::ortho(0.0f, (float)xRes, (float)yRes, 0.0f));
}

void DLib::SpriteRenderer::EnableScissorTest(DLib::Rect region)
{
	if(bufferInserts > 0)
		Draw();

	glScissor(static_cast<GLint>(region.GetMinPosition().x), static_cast<GLint>(resolutionRect.GetHeight() - region.GetMinPosition().y - region.GetHeight()), static_cast<GLsizei>(region.GetWidth()), static_cast<GLsizei>(region.GetHeight()));
	glEnable(GL_SCISSOR_TEST);
}

void DLib::SpriteRenderer::DisableScissorTest()
{
	if(bufferInserts > 0)
		Draw();

	glDisable(GL_SCISSOR_TEST);
}

void DLib::SpriteRenderer::DrawString(const DLib::CharacterSet* characterSet, const DLib::ConstructedString& text, glm::vec2 position, const glm::vec4 color /*= glm::vec4(1.0f, 1.0f, 1.0f, 1.0f)*/)
{
	for(const CharacterBlock& block : text.characterBlocks)
	{
		for(const Character* character : block.characters)
		{
			Draw(*characterSet->GetTexture(), position + glm::vec2(character->xOffset, character->yOffset), DLib::Rect(character->x, character->y, character->width, character->height), color);
			position.x += character->xAdvance;
		}

		position.x += characterSet->GetSpaceXAdvance();
	}
}

void DLib::SpriteRenderer::DrawString(const DLib::CharacterSet* characterSet, const DLib::ConstructedString& text, glm::vec2 position, int maxWidth, const glm::vec4 color /*= glm::vec4(1.0f, 1.0f, 1.0f, 1.0f)*/)
{
	//This method would use an unsigned int for maxWidth but workarounds/hacks should allow a negative width on characters.
	//Besides, 0x7FFFFFFF should be plenty for maxWidth
	int currentWidth = 0;

	for(const CharacterBlock& block : text.characterBlocks)
	{
		for(const Character* character : block.characters)
		{
			Draw(*characterSet->GetTexture(), position + glm::vec2(character->xOffset, character->yOffset), DLib::Rect(character->x, character->y, character->width, character->height), color);
			position.x += character->xAdvance;

			currentWidth += character->xAdvance;
		}

		position.x += characterSet->GetSpaceXAdvance();
		currentWidth += characterSet->GetSpaceXAdvance();

		if(currentWidth >= maxWidth)
			return;
	}
}

void DLib::SpriteRenderer::DrawString(const DLib::CharacterSet* characterSet, const DLib::ConstructedString& text, glm::vec2 position, unsigned int startIndex, unsigned int count, const glm::vec4& color /*= glm::vec4(1.0f, 1.0f, 1.0f, 1.0f)*/)
{
	if(startIndex >= text.text.size() || count <= 0)
		return;

	unsigned int currentIndex = 0;

	//Move to the character block where drawing should begin
	auto blockIter = text.characterBlocks.begin();
	for(; blockIter != text.characterBlocks.end(); ++blockIter)
	{
		if(currentIndex + blockIter->characters.size() < startIndex)
			currentIndex += static_cast<int>(blockIter->characters.size());
		else if(currentIndex + blockIter->characters.size() == startIndex) //startIndex is at the end of the current character block
		{
			position.x += characterSet->GetSpaceXAdvance(); //"Draw" a space
			currentIndex += static_cast<int>(blockIter->characters.size());
		}
		else
			break;

		currentIndex++; //Space
	}

	for(auto end = text.characterBlocks.end(); blockIter != end && currentIndex < startIndex + count; ++blockIter)
	{
		auto characterIter = blockIter->characters.begin();
		if(currentIndex < startIndex) //TODO: Move out of loop?
		{
			characterIter += (startIndex - currentIndex);
			currentIndex = startIndex;
		}

		for(; characterIter != blockIter->characters.end() && currentIndex != startIndex + count; ++characterIter)
		{
			const Character* character = *characterIter;

			Draw(*characterSet->GetTexture(), position + glm::vec2(character->xOffset, character->yOffset), DLib::Rect(character->x, character->y, character->width, character->height), color);
			position.x += character->xAdvance;
			currentIndex++;
		}

		position.x += characterSet->GetSpaceXAdvance();
		currentIndex++; //Space
	}
}

DLib::Texture2D* const DLib::SpriteRenderer::GetWhiteTexture()
{
	return whiteTexture;
}

DLib::Rect DLib::SpriteRenderer::GetWhiteTextureClipRect()
{
	return DLib::Rect();
}