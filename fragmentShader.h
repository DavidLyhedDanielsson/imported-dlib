#ifndef PixelShader_h__
#define PixelShader_h__

#include "content.h"

#include <glm/glm.hpp>
#include <GL/glew.h>

namespace DLib
{
	class FragmentShader :
		public Content
	{
	public:
		FragmentShader();
		~FragmentShader();

		GLuint GetShader() const;

	private:
        bool Load(const std::string& path, DLib::ContentManager* contentManager = nullptr, ContentParameters* contentParameters = nullptr);
        void Unload(DLib::ContentManager* contentManager = nullptr);

		GLuint shader;
	};
}

#endif // PixelShader_h__
